//
//  Login_ViewModel.swift
//  Blackpink
//
//  Created by Nguyen Thanh Sy on 17/02/2022.
//

import Foundation
import RxCocoa
import RxSwift

class LoginViewModel{
   
    let disposeBag = DisposeBag()
    struct Input{
        var userName : Observable<String>
        var passWord : Observable<String>
    }
    struct Output{
        var isHidden : Driver<Bool>
    }
    func bindingData(input: Input) -> Output{
        let checkIsHidden = PublishSubject<Bool>()
        let observable = Observable.combineLatest(input.userName, input.passWord)
        observable.subscribe(onNext: {(userName, passWord) in
            checkIsHidden.onNext(!(userName.isValid(.email)&&(passWord.count>=8)))
                                 }).disposed(by: disposeBag)
        return Output.init(isHidden: checkIsHidden.asDriver(onErrorJustReturn: false))
    }
}
