//
//  Login_ViewController.swift
//  Blackpink
//
//  Created by Nguyen Thanh Sy on 21/01/2022.
//

import UIKit
import RxSwift
import RxCocoa

class Login_ViewController: UIViewController{

    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var lab_user: UILabel!
    @IBOutlet weak var txt_user: UITextField!
//    var checkUsername: Bool = false
    @IBAction func usernameChange(_ sender: Any) {
//        guard let text = txt_user.text else { return }
//        if text.isValid(.email) {
//            checkUsername = true
//            }
//        else {
//            checkUsername = false
//            }
//        if checkUsername && checkPassword {
//            btn_login.isHidden = false
//        }
//        else{
//            btn_login.isHidden = true
//        }
      
    }
    
    
    
    
    @IBOutlet weak var lab_pass: UILabel!
    
    @IBOutlet weak var txt_pass: UITextField!
//    var checkPassword: Bool = false
    @IBAction func passwordChange(_ sender: Any) {
//        guard let text = txt_pass.text else { return }
//        if text.count >= 8 {
//            checkPassword = true
//            }
//        else {
//            checkPassword = false
//            }
//        if checkUsername && checkPassword {
//            btn_login.isHidden = false
//        }
//        else{
//            btn_login.isHidden = true
//        }
    }
    @IBOutlet weak var lab_alert: UILabel!
    
    @IBOutlet weak var btn_login: UIButton!{
        didSet{
            btn_login.isHidden = true
            btn_login.layer.cornerRadius = 10
            btn_login.layer.borderWidth = 2
            btn_login.layer.borderColor = UIColor.white.cgColor
           }
    }
    
    
    
    @IBAction func login(_ sender: Any) {
        if(txt_user.text == "admin@gmail.com") && (txt_pass.text == "12345678"){
            let sc2 = screen2_ViewController()
            
            self.navigationController?.pushViewController(sc2, animated: true)
            
        }
        else{
            lab_alert.isHidden = false
        }
            
    }
    
  
    var loginViewModel = LoginViewModel()
    let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        bindingUI()
    }
    
    func bindingUI(){
        let input = LoginViewModel.Input(userName: self.txt_user.rx.text.orEmpty.asObservable(), passWord: self.txt_pass.rx.text.orEmpty.asObservable())
        
        let output = loginViewModel.bindingData(input: input)
        output.isHidden.drive(self.btn_login.rx.isHidden).disposed(by: disposeBag)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
