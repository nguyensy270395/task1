//
//  screen2_ViewController.swift
//  Blackpink
//
//  Created by Nguyen Thanh Sy on 21/01/2022.
//

import UIKit

class screen2_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {

    

    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var sc2_collection: UICollectionView!
    @IBOutlet weak var sc2_table: UITableView!
    
    var imgcv:[String] = ["lisa","jisoo","rose","jennie"]
    var namecv:[String] = ["Lisa","Jisoo","Rose","Jennie"]
    var imgtv:[String] = []
    var nametv:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        sc2_collectioncode()
        sc2_tablecode()
    
    }

    func sc2_collectioncode(){
        sc2_collection.dataSource = self
        sc2_collection.delegate = self
        sc2_collection.register(UINib(nibName: "cell_cv", bundle: nil), forCellWithReuseIdentifier: "cell_cv")
        sc2_collection.allowsMultipleSelection = true
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 30)
        layout.itemSize = CGSize(width: 110, height: 155)
        layout.minimumInteritemSpacing = 30
        layout.minimumLineSpacing = 30
        layout.scrollDirection = .horizontal
        sc2_collection.collectionViewLayout = layout
        
    }
    func sc2_tablecode(){
        sc2_table.dataSource = self
        sc2_table.delegate = self
        sc2_table.register(UINib(nibName: "cell_tv", bundle: nil), forCellReuseIdentifier: "cell_tv")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_tv") as! cell_tv
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgcv.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_cv", for: indexPath) as! cell_cv
        cell.img.image = UIImage(named: imgcv[indexPath.row])
        cell.name.text = namecv[indexPath.row]
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
