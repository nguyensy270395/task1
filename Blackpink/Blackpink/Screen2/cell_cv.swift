//
//  cell_cv.swift
//  Blackpink
//
//  Created by Nguyen Thanh Sy on 22/01/2022.
//

import UIKit

class cell_cv: UICollectionViewCell {

    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setimg()
    }
    override var isSelected: Bool{
        didSet{
            if (isSelected){
                name.layer.backgroundColor = UIColor.black.cgColor
                name.textColor = UIColor(red: 242/255, green: 166/255, blue: 191/255, alpha: 1)
                img.layer.borderColor = UIColor.black.cgColor
                name.layer.borderColor = UIColor.black.cgColor
                name.layer.borderWidth = 2
                name.layer.cornerRadius = 8
                name.cornerRadius = 8
            }
            else{
                img.layer.borderColor = UIColor.white.cgColor
                name.layer.borderColor = UIColor.white.cgColor
                name.textColor = UIColor.white
                name.layer.borderWidth = 2
                name.layer.cornerRadius = 8
                name.layer.backgroundColor = nil
            }
            
        }
    }
   
    func setimg(){
      
        img.layer.cornerRadius = img.frame.height/2
        img.layer.borderWidth = 2
        img.layer.borderColor = UIColor.white.cgColor
    }
    
    func setlab(){
        
    }

}
